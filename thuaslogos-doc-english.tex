%%
%% thuaslogos.tex - documentation of logos for THUAS - English version
%%
%% Logos for The Hague University of Applied Sciences (THUAS)
%% by Jesse op den Brouw
%%
%% Copyright (c)2019, Jesse E. J. op den Brouw
%%

%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%   http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX 
%% version 2003/12/01 or later.

%% This work consists of the files thuaslogos.sty,
%% thuaslogos.tex

%% This software is provided 'as is', without warranty of any kind,
%% either expressed or implied, including, but not limited to, the
%% implied warranties of merchantability and fitness for a
%% particular purpose.

%% Jesse op den Brouw
%% Department of Electrical Engineering
%% The Hague University of Applied Sciences
%% Rotterdamseweg 137, 2628 AL, Delft
%% Netherlands
%% J.E.J.opdenBrouw@hhs.nl

%% The newest version of this documentclass should always be available
%% from BitBucket: https://bitbucket.org/jesseopdenbrouw/thuaslogos

%% Version 1.2
%%

\documentclass[a4paper,12pt]{article}

\usepackage{thuaslogos}
\usepackage{parskip}
\usepackage[english]{babel}
\usepackage{float}
\usepackage{multicol}
\usepackage{tocloft}
\renewcommand\cftsecleader{\cftdotfill{\cftdotsep}}
%\renewcommand\cftsecafterpnum{\vspace*{-1ex}}
\usepackage{tikz}
\usetikzlibrary{scopes,fadings}
\usepackage[hyphens]{url}
\usepackage[framemethod=tikz]{mdframed}
\usepackage{charter}
\usepackage[left=1.2in,right=1.2in,top=1in,bottom=1.5in,footskip=0.4in]{geometry}


\author{Jesse op den Brouw\thanks{The Hague University of Applied Sciences, \texttt{J.E.J.opdenBrouw@hhs.nl}}}
\title{THUAS logos\\[2ex] \large Logos for The Hague University of Applied Sciences / De Haagse Hogeschool in Dutch and English Engels v\fileversion}
\date{\today}

\begin{document}
\maketitle

\begin{multicols}{2}[\setlength{\columnsep}{30pt}\section*{\contentsname}]
\makeatletter
\@starttoc{toc}%
\makeatother
\end{multicols}
\clearpage


\section{Introduction}
The package \verb|thuaslogos| provides a number of logos of The Hague University of Applied Sciences
(Dutch:  De Haagse Hogeschool) in the colours black, white, grey and and green. The colours grey and
green are specific for THUAS/HHS.

The package can be loaded with the line:

\verb|  \usepackage{thuaslogos}|

The package has no options. Currently the general logos of THUAS and the logos of the
faculties TIS, BFM, ITD, MO, SWE, PLS (BRV) en HNS (GVS) are implemented. Furthermore,
the logos of ``Let's Change'' are implemented.


\section{Where to find}
The latest version will be published at:

\url{https://bitbucket.org/jesseopdenbrouw/thuaslogos}


\section{The used colours}
The Hague University of Applied Sciences uses two primary colours: grey and green.

Grey is Pantone colour 432. This is more or less equal to CYMK (67,45,27,70) and RGB (34,51,67).

Green is Pantone colour 2305. This is more or less equal to CYMK (25,0,100,32) and RGB (158,167,0).

If grey and green are not an option, black can be used. Furthermore, The Hague University of
Applied Sciences uses a number of secondary colours. See Section~\ref{anderekleur}.

If the package \verb|xcolor| is loaded, two colours are defined. These are
\verb|thuasgreen| en \verb|thuasgrey|, defined as follows:

\begin{verbatim}
\definecolor{thuasgreen}{RGB}{158,167,0}
\definecolor{thuasgrey}{RGB}{34,51,67}
\end{verbatim}


\section{Logos HHS Dutch}
The logos are typeset at their natural size. The frame indicates the edges of the logos.
They are not part of the logos.

\begin{verbatim}
\thuaslogodutchblack
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogodutchblack}
\caption{HHS logo Dutch black.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgrey
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogodutchgrey}
\caption{HHS logo Dutch grey.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreen
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogodutchgreen}
\caption{HHS logo Dutch green.}
\end{figure}


\section{Logos THUAS Engish}
The logos are typeset at their natural size. The frames indicate the edges of the logos.
They are not part of the logos.

\begin{verbatim}
\thuaslogoenglishblack
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoenglishblack}
\caption{THUAS logo English black.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgrey
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoenglishgrey}
\caption{THUAS logo English grey.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreen
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoenglishgreen}
\caption{THUAS logo Engish green.}
\end{figure}


\section{Logos TIS Dutch}
The logos of the faculty TIS are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogodutchblacktis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblacktis}}
\caption{TIS logo Dutch black.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreytis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreytis}}
\caption{TIS logo Dutch grey.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreentis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreentis}}
\caption{TIS logo Dutch green.}
\end{figure}


\section{Logos TIS English}
The logos of the faculty TIS are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogoenglishblacktis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblacktis}}
\caption{TIS logo English black.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreytis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreytis}}
\caption{TIS logo Engish grey.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreentis
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreentis}}
\caption{TIS logo Engish green.}
\end{figure}


\section{Logos BFM Dutch}
The logos of the faculty BFM are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogodutchblackbfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackbfm}}
\caption{BFM logo Dutch black.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreybfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreybfm}}
\caption{BFM logo Dutch grey.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenbfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenbfm}}
\caption{BFM logo Dutch green.}
\end{figure}


\section{Logos BFM English}
The logos of the faculty BFM are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogoenglishblackbfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackbfm}}
\caption{BFM logo English black.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreybfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreybfm}}
\caption{BFM logo English grey.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenbfm
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenbfm}}
\caption{BFM logo English green.}
\end{figure}


\section{Logos ITD Dutch}
The logos of the faculty ITD are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogodutchblackitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackitd}}
\caption{ITD logo Dutch black.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreyitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreyitd}}
\caption{ITD logo Dutch grey.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenitd}}
\caption{ITD logo Dutch green.}
\end{figure}


\section{Logos ITD English}
The logos of the faculty ITD are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogoenglishblackitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackitd}}
\caption{ITD logo English black.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreyitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreyitd}}
\caption{ITD logo English grey.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenitd
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenitd}}
\caption{ITD logo Engish green.}
\end{figure}


\section{Logos MO Dutch}
The logos of the faculty MO are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogodutchblackmo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackmo}}
\caption{MO logo Dutch black.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreymo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreymo}}
\caption{MO logo Dutch grey.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenmo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenmo}}
\caption{MO logo Dutch green.}
\end{figure}


\section{Logos MO English}
The logos of the faculty MO are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogoenglishblackmo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackmo}}
\caption{MO logo English black.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreymo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreymo}}
\caption{MO logo English grey.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenmo
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenmo}}
\caption{MO logo English green.}
\end{figure}


\section{Logos SWE Dutch}
The logos of the faculty SWE are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogodutchblackswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackswe}}
\caption{SWE logo Dutch black.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreyswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreyswe}}
\caption{SWE logo Dutch grey}.
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenswe}}
\caption{SWE logo Dutch green.}
\end{figure}


\section{Logos SWE English}
The logos of the faculty SWE are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogoenglishblackswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackswe}}
\caption{SWE logo English black.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreymswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreyswe}}
\caption{SWE logo English grey.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenswe
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenswe}}
\caption{SWE logo English green.}
\end{figure}


\section{Logos BRV (PLS) Dutch}
The logos of the faculty BRV (PLS) are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogodutchblackbrv
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackbrv}}
\caption{BRV logo Dutch black.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreybrv
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreybrv}}
\caption{BRV logo Dutch grey.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreenbrv
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreenbrv}}
\caption{BRV logo Dutch green.}
\end{figure}


\section{Logos PLS (BRV) English}
The logos of the faculty PLS (BRV) are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogoenglishblackpls
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackpls}}
\caption{PLS logo English black.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreympls
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreypls}}
\caption{PLS logo English grey.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenpls
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenpls}}
\caption{PLS logo English green.}
\end{figure}


\section{Logos GVS (HNS) Dutch}
The logos of the faculty GVS (HNS) are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogodutchblackgvs
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchblackgvs}}
\caption{GVS logo Dutch black.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreygvs
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreygvs}}
\caption{GVS logo Dutch grey.}
\end{figure}

\begin{verbatim}
\thuaslogodutchgreengvs
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogodutchgreengvs}}
\caption{GVS logo Dutch green.}
\end{figure}


\section{Logos HNS (GVS) Engels}
The logos of the faculty HNS (GVS) are typeset at the half of their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogoenglishblackhns
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishblackhns}}
\caption{HNS logo English black.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreyhns
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreyhns}}
\caption{HNS logo English grey.}
\end{figure}

\begin{verbatim}
\thuaslogoenglishgreenhns
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\scalebox{0.7071}{\thuaslogoenglishgreenhns}}
\caption{HNS logo English green.}
\end{figure}


\section{Logos Let's change}
These logos are for general purpose and typeset at their natural size.
The frames indicate the edges of the logos. They are not part of the logos.

\begin{verbatim}
\thuaslogoletschangeblack
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangeblack}
\caption{Logo Let's change black.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangegrey
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangegrey}
\caption{Logo Let's change grey.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangegreen
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangegreen}
\caption{Logo Let's change green.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangeframeblack
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangeframeblack}
\caption{Logo Let's change frame black.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangeframegrey
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangeframegrey}
\caption{Logo Let's change frame grey.}
\end{figure}

\begin{verbatim}
\thuaslogoletschangeframegreen
\end{verbatim}

\begin{figure}[H]
\centering\fboxsep=0pt%
\fbox{%
\thuaslogoletschangeframegreen}
\caption{Logo Let's change frame green.}
\end{figure}


\section{Scaling logos}
Logos can be typeset at a certain width or height, by use of the command
\verb|\resizebox|:

\begin{verbatim}
\resizebox{4cm}{!}{\thuaslogodutchgrey}
\end{verbatim}

The exclamation mark in the second argument specifies to keep the aspect ratio.

\begin{figure}[H]
\centering
\resizebox{4cm}{!}{\thuaslogodutchgrey}
\caption{Logo typeset at a width of 4 cm (1.6 in).}
\end{figure}

With the command \verb|\scalebox| can be scaled with respect to the natural size:

\begin{verbatim}
\scalebox{0.7071}{\thuaslogodutchgrey}
\end{verbatim}

\begin{figure}[H]
\centering
\scalebox{0.5}{\thuaslogodutchgrey}
\caption{Logo scaled to the half of the natural size.}
\end{figure}


\section{Ti\emph{k}Z}
Logos can be used in a \verb|tikzpicture| environment. Ti\emph{k}Z options
can be provided.

\begin{verbatim}
\begin{tikzpicture}[scale=0.5,opacity=0.7071]
\newbox\mybox
\node[above,yslant=0.05] at(0,0) {\global\setbox\mybox=
                        \hbox{\thuaslogoenglishgreen}\copy\mybox};
\node[above,yscale=-1,opacity=0.7071,scope fading=south,fading angle=15,
                               yslant=0.05] at(0,0) {\copy\mybox};
\draw[blue,thick,opacity=0.9] (current bounding box.east) -- 
                                        (current bounding box.west);
\end{tikzpicture}
\end{verbatim}

\begin{figure}[H]
\centering
\begin{tikzpicture}[scale=0.5,opacity=0.7071]
\newbox\mybox
\node[above,yslant=0.05] at(0,0){\global\setbox\mybox=\hbox{\thuaslogoenglishgreen}\copy\mybox};
\node[above,yscale=-1,opacity=0.7071,scope fading=south,fading angle=15,yslant=0.05] at(0,0) {\copy\mybox};
\draw[blue,thick,opacity=0.9] (current bounding box.east) -- (current bounding box.west);
\end{tikzpicture}
\caption{Logo scaled to 50\% With a \textsl{slant} of 5\%.}
\end{figure}


\section{Logos with different colours}
\label{anderekleur}
Logos can be typeset with a alternative colour by adjust the colour \verb|thuaslogos@oolor|
and making use of the internal logos. The colours use below are the secondary colour that
The Hague University uses:

\begin{verbatim}
\makeatletter
\definecolor{thuaslogos@color}{RGB}{0,178,205}%
\thuaslogo@logoenglishnocolor \qquad \thuaslogo@logodutchnocolor
\definecolor{thuaslogos@color}{RGB}{202,67,60}%
\thuaslogo@logoenglishnocolortis \qquad \thuaslogo@logodutchnocolortis
\definecolor{thuaslogos@color}{RGB}{255,186,0}%
\thuaslogo@logoenglishnocolorbfm \qquad \thuaslogo@logodutchnocolorbfm
\definecolor{thuaslogos@color}{RGB}{142,152,6}%
\thuaslogo@logoenglishnocoloritd \qquad \thuaslogo@logodutchnocoloritd
\definecolor{thuaslogos@color}{RGB}{168,173,0}%
\thuaslogo@logoenglishnocolormo \qquad \thuaslogo@logodutchnocolormo
\definecolor{thuaslogos@color}{RGB}{59,69,89}%
\thuaslogo@logoenglishnocolorswe \qquad \thuaslogo@logodutchnocolorswe
\definecolor{thuaslogos@color}{RGB}{78,91,118}%
\thuaslogo@logoenglishnocolorpls \qquad \thuaslogo@logodutchnocolorbrv
\makeatother
\end{verbatim}

\begin{figure}[H]
\centering
\makeatletter
\definecolor{thuaslogos@color}{RGB}{0,178,205}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolor \qquad \thuaslogo@logodutchnocolor}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{202,67,60}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolortis \qquad \thuaslogo@logodutchnocolortis}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{255,186,0}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolorbfm \qquad \thuaslogo@logodutchnocolorbfm}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{142,152,6}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocoloritd \qquad \thuaslogo@logodutchnocoloritd}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{168,173,0}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolormo \qquad \thuaslogo@logodutchnocolormo}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{59,69,89}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolorswe \qquad \thuaslogo@logodutchnocolorswe}
\vskip20pt
\definecolor{thuaslogos@color}{RGB}{78,91,118}%
\resizebox{0.9\textwidth}{!}{\thuaslogo@logoenglishnocolorpls \qquad \thuaslogo@logodutchnocolorbrv}
\makeatother
\caption{Logos typeset with alternative colours.}
\end{figure}

It is possible to typeset the logos in white relative to a background colour:

\begin{verbatim}
% thuasgreen defined by package if xcolor is loaded
%\definecolor{thuasgreen}{RGB}{158,167,0}
\begin{mdframed}[hidealllines=true,backgroundcolor=thuasgreen]
\thuaslogoenglishwhite \qquad thuaslogodutchwhite
\end{mdframed}
\end{verbatim}

\begin{figure}[H]
\centering
\begin{mdframed}[hidealllines=true,backgroundcolor=thuasgreen]
\centering
\resizebox{0.9\textwidth}{!}{\thuaslogoenglishwhite \qquad \thuaslogodutchwhite}
\end{mdframed}
\caption{Logos typeset in white.}
\end{figure}


\section{Generation of logos, rendering speed}
The logos are supplied by the department Communication and Marketing. The original logos
are in \verb|eps| format and converted to \verb|PGF| commands with the tool
\verb|eps2pfg|\footnote{See \url{https://sourceforge.net/projects/eps2pgf/}}. This
package will load the \verb|pgf| package.

It is possible to convert the \verb|eps| files in InkScape to Ti\emph{k}Z format. The
size of the files is less than the file size of the \verb|PGF| versions but Ti\emph{k}Z
has to be loaded. Although this package consists of more that 12,500 lines, rendering
with PGF is much quicker than with Ti\emph{k}Z. All Ti\emph{k}Z commandos have to be
converted to suitable PGF commandos and that takes a lot of time.


\section{All macros at one glimpse}
\begin{verbatim}
\thuaslogodutchblack
\thuaslogodutchblackbfm
\thuaslogodutchblackbrv
\thuaslogodutchblackgvs
\thuaslogodutchblackitd
\thuaslogodutchblackmo
\thuaslogodutchblackswe
\thuaslogodutchblacktis
\thuaslogodutchgreen
\thuaslogodutchgreenbfm
\thuaslogodutchgreenbrv
\thuaslogodutchgreengvs
\thuaslogodutchgreenitd
\thuaslogodutchgreenmo
\thuaslogodutchgreenswe
\thuaslogodutchgreentis
\thuaslogodutchgrey
\thuaslogodutchgreybfm
\thuaslogodutchgreybrv
\thuaslogodutchgreygvs
\thuaslogodutchgreyitd
\thuaslogodutchgreymo
\thuaslogodutchgreyswe
\thuaslogodutchgreytis
\thuaslogodutchwhite
\thuaslogodutchwhitebfm
\thuaslogodutchwhitebrv
\thuaslogodutchwhitegvs
\thuaslogodutchwhiteitd
\thuaslogodutchwhitemo
\thuaslogodutchwhiteswe
\thuaslogodutchwhitetis
\thuaslogoenglishblack
\thuaslogoenglishblackbfm
\thuaslogoenglishblackhns
\thuaslogoenglishblackitd
\thuaslogoenglishblackmo
\thuaslogoenglishblackpls
\thuaslogoenglishblackswe
\thuaslogoenglishblacktis
\thuaslogoenglishgreen
\thuaslogoenglishgreenbfm
\thuaslogoenglishgreenhns
\thuaslogoenglishgreenitd
\thuaslogoenglishgreenmo
\thuaslogoenglishgreenpls
\thuaslogoenglishgreenswe
\thuaslogoenglishgreentis
\thuaslogoenglishgrey
\thuaslogoenglishgreybfm
\thuaslogoenglishgreyhns
\thuaslogoenglishgreyitd
\thuaslogoenglishgreymo
\thuaslogoenglishgreypls
\thuaslogoenglishgreyswe
\thuaslogoenglishgreytis
\thuaslogoenglishwhite
\thuaslogoenglishwhitebfm
\thuaslogoenglishwhitehns
\thuaslogoenglishwhiteitd
\thuaslogoenglishwhitemo
\thuaslogoenglishwhitepls
\thuaslogoenglishwhiteswe
\thuaslogoenglishwhitetis
\thuaslogoletschangeblack
\thuaslogoletschangeframeblack
\thuaslogoletschangeframegreen
\thuaslogoletschangeframegrey
\thuaslogoletschangegreen
\thuaslogoletschangegrey
\end{verbatim}


\begin{multicols}{2}[\setlength{\columnsep}{30pt}\section{Changelog}]
\begin{itemize}
\item[v1.2] [2019/06/07]\\ English documentation.
\item[v1.1] [2019/01/09]\\ Logos of BFM, ITD, MO, SWE, BRV (PLS) en GVS (HNS) added. Logos Let's change added. Colour of logos adjusted to corporate style. Logos are \verb|tikzpicture|-aware. Logos in white.
\item[v1.0] [2019/01/01]\\ Initial release. Logos of HHS/THUAS and TIS.
\end{itemize}
\end{multicols}



\end{document}