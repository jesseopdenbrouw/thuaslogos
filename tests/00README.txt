In Windows, start Power Shell.

Then, on command line, type:

	Measure-Command { pdflatex -interaction=nonstopmode testpgf.tex }

to measure PGF rendering of logos.

To measure TikZ rendering of logos, type:

	Measure-Command { pdflatex -interaction=nonstopmode testtikz.tex }

Substitute pdflatex with your favorite engine


JodB, 8-1-2018